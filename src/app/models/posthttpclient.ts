
export interface PostHttpClient {
    id: number,
    title: string,
    body: string
}
