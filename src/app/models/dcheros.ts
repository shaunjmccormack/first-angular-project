export interface dchero {
    persona: string,
    firstName: string,
    lastName: string,
    age: number,
    address?: {
        street: any,
        city?: any,
        state?: any,
    },
    img?: string,
    isActive?: boolean,
    balance?: number,
    memberSince?: any,
    hide?: boolean,
}
