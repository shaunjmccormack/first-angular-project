// interface means blueprint
// the model is always singular. because it represents the data of one object. More than one model is a colection.
export interface movie {
title: string,
yearReleased: number,
 actors: object,
 director:string,
 genre: string, 
 rating: string, 
 awards?: object
 img?: string,
}