import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import { Comment } from '../../app/models/Comment';



const httpOptions = {
  headers: new HttpHeaders ({"Content-type": "application/json"})
}

@Injectable({
  providedIn: 'root'
})

// updatePost(post: PostHttpClient): Observable<PostHttpClient> {
//   const url = `${this.postsUrl}/${post.id}`
// return this.httpClient.put<PostHttpClient>(url, post, httpOptions)
// }


export class CommentsService {


   updateComment(comment: Comment): Observable<Comment> {
     const url = `${this.commentsUrl}/${comment.id}`
    return this.httpClient.put<Comment>(url, comment, httpOptions)
  }
 

commentsUrl: string = "https://jsonplaceholder.typicode.com/comments"

  constructor(private httpClient: HttpClient) { }

  getComment():Observable<Comment[]>{
    return this.httpClient.get<Comment[]>(this.commentsUrl)
  }

  saveComment(comment: Comment): Observable<Comment>{
    return this.httpClient.post<Comment>(this.commentsUrl, comment, httpOptions)
  }


}
