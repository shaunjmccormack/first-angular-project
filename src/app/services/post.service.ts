import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostHttpClient} from "../models/PostHttpClient";

@Injectable({
  providedIn: 'root'
})

export class PostsService {
  // Properties
  // set a url as a property
  postsUrl: string = "https://jsonplaceholder.typicode.com/posts";

  // inject the HttpClientModule as a dependency
  constructor(private httpClient: HttpClient) { }

  // create a method that will make our GET request to the postsUrl
  getPosts() : Observable<PostHttpClient[]> {
    
    // returning all the data that comes with our postsUrl property
    return this.httpClient.get<PostHttpClient[]>(this.postsUrl);
  }
}