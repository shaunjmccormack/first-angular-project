import { Injectable } from '@angular/core';
import {dchero} from "../models/dcheros";
import {Observable, of} from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class DataService {
  // PROPERTIES
  // initialize the characters property and assign it to an array of our
  // DCHero interface
  characters: dchero[] = [];
  constructor() {
    this.characters = [
      {
        persona: "Superman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: '27 Smallville',
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/644-superman.jpg.jpg",
        isActive: true,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Wonder Woman",
        firstName: "Diana",
        lastName: "Prince",
        age: 5000,
        address: {
          street: '150 Main St',
          city: "Los Angeles",
          state: "CA"
        },
        img: "../assets/img/720-wonder-woman.jpg",
        balance: 14000000,
        memberSince: new Date("07/11/2005 10:45:00"),
        hide: false
      },
      {
        persona: "Batman",
        firstName: "Bruce",
        lastName: "Wayne",
        age: 43,
        address: {
          street: '50 Wayne Manor',
          city: "Gotham City",
          state: "NJ"
        },
        img: "../assets/img/70-batman.jpg",
        isActive: true,
        balance: 20000000,
        memberSince: new Date("01/01/1937 7:15:00"),
        hide: false
      }
    ]; // end of array
  } // end of constructor
  // METHODS
  // create a method that will return an array of this.characters
  // using an Observable
  // OBSERVABLES
  /*
  - Provides support for passing messages/data between parts of your application
  - Used frequently and is a technique for event handling, asynchronous programming,
  and handling multiple values
   */
  getCharacters() : Observable<dchero[]> {
    console.log("Getting characters from dataService")
    return of(this.characters);
  }
} // end of class