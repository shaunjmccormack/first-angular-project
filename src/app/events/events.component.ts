import { Component, OnInit } from '@angular/core';
import { dchero } from '../models/dcheros';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
 showForm: boolean = true

 characters: dchero[];
 enableAddUser: boolean;
 currentClasses: {};
 currentStyle: {};


  constructor() { }

  ngOnInit(): void {

    this.characters = [
    {
      persona: 'Superman',
      firstName: 'Clark',
      lastName: 'Kent',
      age: 100000,
      address: {
          street: '278 Smallville',
          city: 'Metropolis',
          state: 'IL'
      },
      img: '',
      isActive: true,
      balance: 132413241234,
      memberSince: new Date('05/01/1930 8:30:00')
  },
  {
      persona: 'Wonder Woman',
      firstName: 'Diana',
      lastName: 'Prince',
      age: 100000,
      address: {
          street: '278 Smallville',
          city: 'Metropolis',
          state: 'IL'
      },
      img: '',
      isActive: true,
      balance: 132413241234,
      memberSince: new Date('05/01/1930 8:30:00')
  },
  {
      persona: 'Batman',
      firstName: 'Bruce',
      lastName: 'Wayne',
      age: 100000,
      address: {
          street: '278 Smallville',
          city: 'Metropolis',
          state: 'IL'
      },
      img: '',
      isActive: true,
      balance: 132413241234,
      memberSince: new Date('05/01/1930 8:30:00')
  },
]
this.setCurrentClasses();
this.setCurrentStyle();

}
setCurrentClasses() {
  this.currentClasses = {
    'btn-success': this.enableAddUser
  }
}
setCurrentStyle() {
 this.currentStyle = {
   'padding-top': '60px',
   'text-decoration': 'underline'
 }
}

 toggleInfo(character:any){
  character.hide = !character.hide
}


  toggleForm(){
    console.log("toggle");
    this.showForm = !this.showForm;
  }

  triggerEvent(e: any){
    console.log(e.type);
  }
  
  // fireEvent(e.any){
    // console.log(e.type);
    // get input value
    // console.log(e.target.val);
  // }
}
