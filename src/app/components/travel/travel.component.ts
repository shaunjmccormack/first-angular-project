import { Component, OnInit} from '@angular/core';

//This is the component decorator
@Component({
    selector: 'app-travel',
    templateUrl: './travel.component.html',
    styleUrls: ['./travel.component.css'],
})

// This is a class.  I am exporting the class i just made. so that it is accesable and can be imported by other components.
export class TravelComponent implements OnInit{

        ngOnInit() {

        }
}