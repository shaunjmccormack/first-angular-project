import { Component, OnInit } from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostsService} from "../../services/post.service";

@Component({
  selector: 'app-posthttpclient',
  templateUrl: './posthttpclient.component.html',
  styleUrls: ['./posthttpclient.component.css']
})
export class PostHttpClientComponent implements OnInit {
  // property
  posts: PostHttpClient[];

  currentPost: PostHttpClient = {
    id: 0,
    title: '',
    body: ''
  };

  // property for our update button
  isEdit: boolean = false;

  // inject our service as a dependency
  // Dependency Injection
  constructor(private postService: PostsService) { }

  // fetch the posts when ngOnInit() method is initialized
  ngOnInit(): void {

    // subscribe to our Observable that's in our PostService
    this.postService.getPosts()
      .subscribe(p => {
        // console.log(p);
        this.posts = p;
      })
  }
  
  // create onNewPost() method
  onNewPost(post: PostHttpClient) {
    this.posts.unshift(post);
  }
  // create the method for onUpdatedPost()
  onUpdatedPost(post: PostHttpClient) {
    this.posts.forEach((current, index) => {
      if (post.id === current.id) {
        this.posts.splice(index, 1);
        this.posts.unshift(post);
        this.isEdit = false;
        this.currentPost = {
          id: 0,
          title: '',
          body: ''
        }
      }
    })
  }
  // create editPost() method
  editPost(post: PostHttpClient) {
    this.currentPost = post;
    // included for the update button
    this.isEdit = true;
  }
  // create the method for removePost()
  // removePost(post: PostHttpClient) {
  //   if (confirm("Are you sure?")) {
  //     this.postService.deletePost(post.id)
  //       .subscribe(() => {
  //         this.posts.forEach((current, index) => {
  //           if (post.id === current.id) {
  //             this.posts.splice(index, 1);
  //           }
  //         })
  //       })
  //   }
  // }
}