import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosthttpclientComponent } from './posthttpclient.component';

describe('PosthttpclientComponent', () => {
  let component: PosthttpclientComponent;
  let fixture: ComponentFixture<PosthttpclientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosthttpclientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosthttpclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
