import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  show:boolean = false;
  displayDetails:any = 'Show';
  button_clicks : Array <number> = [] ;
  clicks: number = 0
  
  constructor() { }

  ngOnInit(): void {
  }

  toggle() {
    this.show = !this.show;
  }

}
