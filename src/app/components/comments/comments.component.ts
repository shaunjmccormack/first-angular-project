
import { Component, OnInit } from '@angular/core';
import { Comment } from '../../models/Comment';
import { CommentsService } from '../../services/comments.service'

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  comments: Comment[];

  selectedComment: Comment = {
    id: 0,
    email: '',
    name: '',
    body: '',
  }

  isEdit: boolean = false

  // selectedComment: { new(data?: string): globalThis.Comment; prototype: globalThis.Comment; };


  constructor ( private commentService: CommentsService ) { }


  ngOnInit(): void {

this.commentService.getComment().subscribe(c => 
  {this.comments = c;})

}
  onNewComment(comment: Comment){
    this.comments.unshift(comment)
  }

onUpdatedComment(comment: Comment) {
  this.comments.forEach(( current, index ) => {
    if (comment.id === current.id) {
      this.comments.splice(index, 1);
      this.comments.unshift(comment);
      this.isEdit = false;
      this.selectedComment = {
        id: 0,
        email: '',
        name: '',
        body: '',
      }
    }
  })
}
editComment(comment: Comment) {
  this.selectedComment = comment

this.isEdit = true
}
 // create the method for removePost()
  // removePost(post: PostHttpClient) {
  //   if (confirm("Are you sure?")) {
  //     this.postService.deletePost(post.id)
  //       .subscribe(() => {
  //         this.posts.forEach((current, index) => {
  //           if (post.id === current.id) {
  //             this.posts.splice(index, 1);
  //           }
  //         })
  //       })
  //   }
  // }


}
