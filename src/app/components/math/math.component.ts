import { Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-math',
    templateUrl: './math.component.html',
    styleUrls: ['./math.component.css'],
})
export class MathComponent implements OnInit{


    //these are your properties
        num1: number
        num2: number
        num3: number
        num4: number
        num5: number
        num6: number
        num7: number
        num8: number
    
    //these are your functions

    constructor(){


    } 

        addNum(num1, num2){
            console.log(num1 + num2)
        }

        differenceOf(num3, num4){
            if (num3 <= num4){
            console.log (num4 - num3)
        }else{
            console.log(num3 - num4)
            }
        }

        productOf(num5, num6){
            console.log(num5 * num6)
        }
        
        quotientOf(num7, num8){
            if (num7 <= num8){
            console.log (num8 / num7)
        }else{
            console.log(num7 / num8)
            }
        }
    
        
    //this is your bonus function.  this is also a job interview question.     
        fizzBuzz(){
            for(let i = 1; i < 101; i++){
                if (i % 3 === 0 && i % 5 === 0){  
                console.log('fizzBuzz')
                } else if (i % 3 === 0){
                    console.log('buzz')
                } else if (i % 5 === 0){
                    console.log('fizz')
                }
            }
        }

        ngOnInit() {
        }
    }

