import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { movie } from 'src/app/models/movie';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})

export class MoviesComponent implements OnInit {

movies: movie[] =[]
// enableCheckMovie: boolean = true



  constructor() { }

  ngOnInit(): void {
  

    this.movies =[
      {
        title: 'The Shawshank Redemption',
        yearReleased:1994,
        actors: ['Tim Robbins', 'Morgan Freeman', 'Bob Gunton'],
        director: 'Frank Darabont',
        genre: 'Drama',
        rating:'R',
        awards:['best adapted screen play', 'outstding performance by a male actor in a leading role', 'best cinematography'],
        img: '../../assets/'
      },
      {
        title: 'The Shaw Redemption',
        yearReleased:1994,
        actors: ['Tim Robbins', 'Morgan Freeman', 'Bob Gunton'],
        director: 'Frank Darabont',
        genre: 'Drama',
        rating:'R',
        awards:['best adapted screen play', 'outstding performance by a male actor in a leading role', 'best cinematography'],
      }
    ]
  
  }
 

//   addMovie(){
//      this.messageEvent.push({

//      })
// }

}

