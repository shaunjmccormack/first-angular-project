
import {Component, OnInit, ViewChild} from '@angular/core';
import {dchero} from "../../models/dcheros";
import {DataService} from "../../services/data.service";
@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
  // PROPERTIES
  characters: dchero[] = [];
  currentClasses: {} = {}; // an empty object
  currentStyle: {} = {};
  enableAddCharacter: boolean = true;
  // character empty object
  character: dchero = {
    persona: '',
    firstName: '',
    lastName: '',
    age: 0,
    address: {
      street: '',
      city: '',
      state: ''
    },
    img: '',
    isActive: true,
    memberSince: '',
    hide: true
  }
  // SUBMIT LESSON
  @ViewChild("characterForm") form: any;
  /*
  Calling ViewChild and passed in the name of our form "characterForm"
  - making this our "Form Identifier"
   */
  constructor(private dataService: DataService) { }
    /*
  Dependency Injection
  private = cannot be used anywhere else, only within this class
  dataService = variable name for our DataService
  DataService = setting our variable to the DataService that we brought in
  Now we should be able to access any method(s) inside of our DataService
   */
  ngOnInit(): void {
       // access the getCharacters() method that's inside of our DataService
      this.dataService.getCharacters().subscribe(data => {
      this.characters = data;
      });
      // whenever we are accessing a Observable method,
      // we need to subscribe to it.
  
    this.setCurrentClasses();
    this.setCurrentStyle();
  } // end of ngOnInit()
  // METHODS
  // example of ngClass
  setCurrentClasses() {
    // calling and updating currentClasses
    this.currentClasses = {
      'btn-success' : this.enableAddCharacter
    }
  }
  // example of ngStyle
  setCurrentStyle() {
    // call and update the property currentStyle
    this.currentStyle = {
      "padding-top" : "60px",
      "text-decoration" : "underline"
    }
  }
  // method to toggle the individual character's info
  toggleInfo(character: any) {
    console.log("toggle info clicked");
    character.hide = !character.hide;
  }
  onSubmit({value, valid} : {value: dchero, valid: boolean}) {
    if (!valid) {
      alert("Form is not valid");
    }
    else {
      value.isActive = true;
      value.memberSince = new Date();
      value.hide = false;
      console.log(value);
      // add the new data to the top of the list
      this.characters.unshift(value);
      // reset the form / clear out the inputs
      this.form.reset();
    }
  }
}


  // ngOnInit(): void {
  //   // add data to the array of characters
  //   this.characters = [
  //     {
  //       persona: "Superman",
  //       firstName: "Clark",
  //       lastName: "Kent",
  //       age: 54,
  //       address: {
  //         street: '27 Smallville',
  //         city: "Metropolis",
  //         state: "IL"
  //       },
  //       img: "../assets/img/644-superman.jpg.jpg",
  //       isActive: true,
  //       memberSince: new Date("05/01/1939 8:30:00"),
  //       hide: false
  //     },
  //     {
  //       persona: "Wonder Woman",
  //       firstName: "Diana",
  //       lastName: "Prince",
  //       age: 5000,
  //       address: {
  //         street: '150 Main St',
  //         city: "Los Angeles",
  //         state: "CA"
  //       },
  //       img: "../assets/img/720-wonder-woman.jpg",
  //       balance: 14000000,
  //       memberSince: new Date("07/11/2005 10:45:00"),
  //       hide: false
  //     },
  //     {
  //       persona: "Batman",
  //       firstName: "Bruce",
  //       lastName: "Wayne",
  //       age: 43,
  //       address: {
  //         street: '50 Wayne Manor',
  //         city: "Gotham City",
  //         state: "NJ"
  //       },
  //       img: "../assets/img/70-batman.jpg",
  //       isActive: true,
  //       balance: 20000000,
  //       memberSince: new Date("01/01/1937 7:15:00"),
  //       hide: false
  //     }
  //   ]; // end of array








