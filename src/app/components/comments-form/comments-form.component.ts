import { Component, OnInit,EventEmitter, Output, Input } from '@angular/core';
import { Comment } from "../../models/Comment";
import { CommentsService } from  "../../services/comments.service";




 @Component({
   selector: 'app-comment-form',
  templateUrl: './comments-form.component.html',
  styleUrls: ['./comments-form.component.css']
})




export class CommentsFormComponent implements OnInit {

  comment: Comment = {
    id: 0,
    email: '',
    name: '',
    body: '',
  }

  @Output() updatedComment: EventEmitter<Comment> = new EventEmitter();

  @Output() newComment: EventEmitter<Comment> = new EventEmitter();

  @Input() currentComment: Comment ={
    id: 0,
    email: '',
    name: '',
    body: '',
  }

  @Input() isEditing: boolean = false;


  constructor (private commentService: CommentsService) { }



  ngOnInit(): void {
  }



  addComment(email: string, name: string, body:string){
  if (!email || !name || !body){
    alert("please Complete Form!!!")
  } else {
    this.commentService.saveComment ({ email, name, body } as Comment ) .subscribe(data => this.newComment.emit(data))
  }
}



  updateComment() {
    console.log("Updating post...");
    this.commentService.saveComment(this.currentComment)
    .subscribe( data => {
      this.isEditing = false;
      this.updatedComment.emit(data)
    })
    this.currentComment = {
        id: 0,
        email: '',
        name: '',
        body: '',
      }
    }

}



