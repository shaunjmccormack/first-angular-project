import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component'
import { HelloComponent } from './components/hello/hello.component'
import { TravelComponent} from './components/travel/travel.component'
import { MathComponent } from './components/math/math.component';
import { VipComponent } from './components/vip/vip.component';
import { BindingComponent } from './components/binding/binding.component';
import { MoviesComponent } from './components/movies/movies.component';
import { EventsComponent } from './events/events.component';
import { FormsComponent } from './components/forms/forms.component';
import { FormsModule } from '@angular/forms';
import { DirectivesComponent } from './components/directives/directives.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PostHttpClientComponent } from './components/posthttpclient/posthttpclient.component';
import { NgmodelComponent } from './components/ngmodel/ngmodel.component';
import { HttpClientModule } from "@angular/common/http";
import { CommentsComponent } from './components/comments/comments.component';
import { CommentsFormComponent } from './components/comments-form/comments-form.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HelloComponent,
    TravelComponent,
    MathComponent,
    VipComponent,
    BindingComponent,
    MoviesComponent,
    EventsComponent,
    FormsComponent,
    DirectivesComponent,
    NavbarComponent,
    PostHttpClientComponent,
    NgmodelComponent,
    CommentsComponent,
    CommentsFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
